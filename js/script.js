$("#menu").hide();
$(".sous_menu").hide();
$(document).ready(function() {
    

	var etat_menu = 0;
	$("#but_menu").click(function() {
		if(etat_menu === 0) {
			setTimeout(function() {
				$("#menu").slideDown();
			}, 100);
			
			etat_menu = 1;
		} else {
			$("#menu").slideUp();
			etat_menu = 0;
		}
	});
	var etat_menu_photo = 0;
	$(".but_aide").click(function() {
		if(etat_menu_photo === 0) {
			$(".sous_menu").slideDown();
			etat_menu_photo = 1;
		} else {
			$(".sous_menu").slideUp();
			etat_menu_photo = 0;
		}
	});
});