region_js = {

	"Title" : "region_JSON",
	"region":[
	{
		"name" : "Auvergne-Rhône-Alpes",
		"aide_regional":[
		[
		"Bourse Mobilité Internationale Lycéens et apprentis",
		"Aux lycéens et apprentis souhaitant bénéficier d’une formation ou d’un stage à l’étranger",
		"90 à 290€",
		"BMILA",
		"https://www.auvergnerhaonealpes.fr/aide/93/89-bourse-region-mobilite-internationale-lyceens-et-apprentis-education-lycees.htm"
		],
		[
		"Bourse Mobile International étudiatns et apprentis du supérieur",
		"Étudiants et apprentis du supérieur inscrits en Auvergne-Rhône-Alpes.",
		"95 à 1360€",
		"MI (AVA)",
		"https://www.auvergnerhonealpes.fr/aide/73/89-se-former-a-l-etranger-avec-la-bourse-regionale-mobilite-internationale-etudiant-enseignement-superieur-recherche-innovation.htm"
		],
		[
		"Bourse sanitaire et social",
		"Personnes inscrites dans un établissement de formation situé dans la région",
		"1009 à 5551€€",
		"BSS (AVA)",
		"https://www.auvergnerhonealpes.fr/aide/47/89-bourse-regionale-pour-les-formations-sante-social-formations-apprentissage.htm"
		],
		[
		"Bourse au mérite",
		"bacheliers ou apprentis les plus brillants ayant opté pour une formation professionnelle au cours de leur scolarité.",
		"500€",
		"BM (AVA)",
		"https://bourseaumerite.auvergnerhonealpes.eu/bourses/jsp/nouveauContexte.action?codeAction=M42-ACCUEIL"
		]
		]
	},
	{
		"name" : "Bourgogne-Franche-Comté",
		"aide_regional":[
		[
		"Mobilité internationale des jeunes",
		"jeunes de moins de 30 ans à l’étranger.",
		"4 800€",
		"MIJ (BFC)",
		"https://www.bourgognefranchecomte.fr/mobilite-internationale"
		]
		]
	},
	{
		"name" : "Bretagne",
		"aide_regional":[
		[
		"Bourse Sanitaire et Sociale",
		"Personnes inscrites dans un établissement de formation situé dans la région",
		"150 à 5500€",
		"BSS (B)",
		"https://www.bretagne.bzh/jcms/prod_223109/fr/bourse-sanitaire-sociale"
		],
		[
		"Bourse Mobilité Internationale",
		"aux lycéens et apprentis souhaitant bénéficier d’une formation ou d’un stage à l’étranger",
		"460€",
		"BMI (B)",
		"https://www.bretagne.bzh/jcms/prod_187189/fr/bourse-jeunes-a-l-international"
		]
		]
	},
	{
		"name" : "Centre-Val de Loire",
		"aide_regional":[
		[
		"Bourse Sanitaire et Sociale",
		"Personnes inscrites dans un établissement de formation situé dans la région",
		"1009 à 5551€",
		"BSS (CVLL)",
		"http://www.regioncentre-valdeloire.fr/accueil/les-services-en-ligne/la-region-vous-aide/education-formation/aide-a-la-formation-sanitaire-et.html"
		],
		[
		"Aide mutuelle étudiante",
		"pour les jeunes ayant du mal à régler le paiement de leurs charges indispensables, notamment les frais médicaux",
		"100€",
		"AME (CVLL)",
		"https://www.lmde.fr/mutuelle-sante/bien-choisir/les-aides/les-aides-regionales/aide-de-la-region-centre-val-de-loire"
		]
		]
	},
	{
		"name" : "Corse",
		"aide_regional":[
		[
		"N/A",
		"N/A",
		"N/A",
		"N/A",
		"N/A"
		]
		]
	},
	{
		"name" : "Grand Est",
		"aide_regional":[
		[
		"Bourse Sanitaire et Sociale",
		"Personnes inscrites dans un établissement de formation situé dans la région",
		"1009 à 5551€",
		"BSS (GE)",
		"https://www.grandest.fr/formations-sanitaires-sociales/bourses-secteur-sanitaire-social/"
		],
		[
		"Aide à la mobilité internationale",
		"Aux lycéens et apprentis souhaitant bénéficier d’une formation ou d’un stage à l’étranger",
		"200 à 800€",
		"AMI (GE)",
		"https://www.grandest.fr/vos-aides-regionales/aide-a-mobilite-internationale-etudiants/"
		]
		]
	},
	{
		"name" : "Hauts-de-France",
		"aide_regional":[
		[
		"Bourse Sanitaire et Sociale",
		"étudiants et des élèves inscrits dans des écoles ou instituts agréés par la Région",
		"1009 à 5551€",
		"BSS(HF)",
		"https://www.hautsdefrance.fr/bourses-formations-sanitaires-sociales/"
		]
		]
	},
	{
		"name" : "Île-de-France",
		"aide_regional":[
		[
		"Aide Installation Logement Etudiants",
		"Pour tout etudiants boursiers qui recherchent un appartement dans le parc privé parisien",
		"900€",
		"AILE (IF)",
		"http://www.crous-paris.fr/logements/aides-de-ville-de-paris/la-i-l-e/"
		],
		[
		"Soutien aux conditions de vie des apprentis",
		"Pour tout etudiant en formation d'apprentissage",
		"100 à 320€",
		"SCVA (IF)",
		"https://www.oriane.info/soutien-aux-conditions-de-vie-des-apprentis"

		],
		[
		"Aide à la mobilité internationale - AMIE",
		"Le programme consiste à financer le séjour de ces étudiants à l’étranger durant leurs études ou leur stage. ",
		"250 à 450€",
		"AMIE (IF)",
		"https://www.iledefrance.fr/bourses-mobilite-ile-de-france-etudiants-2018-2019"
		],
		[
		"Bourse Sanitaire et Sociale",
		"étudiants et des élèves inscrits dans des écoles ou instituts agréés par la Région",
		"1009 à 5551",
		"BSS (IF)",
		"https://www.iledefrance.fr/formations-sanitaires-et-sociales"
		],
		[
		"Bourse au mérite",
		"la Région Ile-de-France cherche à valoriser ses jeunes par une aide financière destinée aux bacheliers qui obtiennent une mention TB au bac.",
		"1000€",
		"BM (IF)",
		"http://www.crous-paris.fr/bourses/laide-au-merite/"
		]
		]
	},
	{
		"name" : "Normandie"
	},
	{
		"name" : "Nouvelle-Aquitaine",
		"aide_regional":[
		[
		"Bourse Ordinaire",
		"Pour tout etudiant de la région éligible",
		"100 à 555€",
		"BO (NA)",
		"https://les-aides.nouvelle-aquitaine.fr/economie-et-emploi/bourses-detudes-sur-criteres-sociaux-etudiants-en-formations-sociales-paramedicales-et-de-sante"
		],
		[
		"Aide à la mobilité internationale",
		"aux lycéens et apprentis souhaitant bénéficier d’une formation ou d’un stage à l’étranger",
		"3840 à  4800€",
		"AMI (NA)",
		"https://les-aides.nouvelle-aquitaine.fr/mobilite-internationale"
		],
		[
		"Bourse Sanitaire et Sociale",
		"étudiants et des élèves inscrits dans des écoles ou instituts agréés par la Région",
		"1424 à 4019€",
		"BSS (NA)",
		"https://www.nouvelle-aquitaine.fr/dispositifs-region/bourses-regionales-sanitaire-social.html#gref"
		],
		[
		" Aide aux stages d'études à l'étranger",
		"Le programme de bourses de stages vise à faciliter la participation des étudiants à l'apprentissage par l'expérience et à l'exploration de carrières",
		"700 à 2900€",
		"ASEE(NA)",
		"https://les-aides.nouvelle-aquitaine.fr/jeunesse/stages-letranger-public-post-bac"
		]
		]
	},
	{
		"name" : "Occitanie"
	},
	{
		"name" : "Pays de la Loire",
		"aide_regional":
		[
		[
		"Bourse d'études Envoléo",
		"Cette bourse d'études est destinée à tous les étudiants âgés moins de 28 ans dans un établissement de l’enseignement supérieur régional.",
		"500 à 3000€",
		"BEE (PL)",
		"http://www.envoleo.paysdelaloire.fr/"
		],
		[
		"Bourse Sanitaire et Sociale",
		"étudiants et des élèves inscrits dans des écoles ou instituts agréés par la Région",
		"1009 à 5551€",
		"BSS (PL)",
		"https://www.paysdelaloire.fr/politiques-regionales/sanitaire-et-social/actu-detaillee/n/les-bourses-sanitaires-et-sociales/"
		],
		[
		"Aide au permis de conduire - Pass permis",
		"Tout étudiant de la région",
		"400€",
		"APC (PL)",
		"https://www.infos-jeunes.fr/se-deplacer/passer-son-permis/les-aides-au-permis-de-conduire-en-pays-de-la-loire"
		]
		]
	},
	{
		"name" : "Provence-Alpes-Côte d'Azur",
		"aide_regional":[
		[
		"N/A",
		"N/A",
		"N/A",
		"N/A",
		"N/A"
		]
		]
	},
	{
		"name" : "Guadeloupe",
		"aide_regional":[
		[
		"N/A",
		"N/A",
		"N/A",
		"N/A",
		"N/A"
		]
		]
	},
	{
		"name" : "Martinique",
		"aide_regional":[
		[
		"N/A",
		"N/A",
		"N/A",
		"N/A",
		"N/A"
		]
		]
	},
	{
		"name" : "Guyane",
		"aide_regional":[
		[
		"N/A",
		"N/A",
		"N/A",
		"N/A",
		"N/A"
		]
		]
	},
	{
		"name" : "La Réunion",
		"aide_regional":  [
		[
		"Allocation stage - ASPM",
		"Tout étudiant de la région",
		"4800 à 7200€",
		"AS (R)",
		"https://www.regionreunion.com/aides-services/article/allocation-de-stages-pratiques-en-mobilite-aspm"


		],
		[
		"Allocation de Premier Equipement (APE)",
		"Cette allocation ouvert a tout étudiant facilite l’acquisition des équipements, des livres, des matériels multimédias, les frais de cours par correspondance ou les cours linguistiques.",
		"300 à 500 €",
		"APE (R)",
		"https://www.regionreunion.com/aides-services/article/allocation-de-premier-equipement-ape-mobilite",
		]
		]
	},
	{
		"name" : "Mayotte",
		"aide_regional":[
		[
		"N/A",
		"N/A",
		"N/A",
		"N/A",
		"N/A"
		]
		]
	}
	]
}