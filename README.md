Fichier d'explication du projet du groupe Polto_Gang.

GitLab: https://gitlab.com/mathis.verleene/polto_gang/

Lien des défis:
- Défi de la nuit 2019 : https://www.nuitdelinfo.com/inscription/defis/174
- Dockerisation : https://www.nuitdelinfo.com/inscription/defis/220
- Selfi : https://www.nuitdelinfo.com/inscription/defis/258

La nuit de l’info 2019 se voit accompagnée du thème de la précarité étudiante, à ce jour près de 10% d’étudiants sont confrontés à ce genre de situation. Ainsi, les données recueillies par notre site peuvent avoir un véritable impact pour régler ce soucis qui engendre chaque année un fort pic de décrochage scolaire ou réduit considérablement la chance de réussir dans l’enseignement supérieur de ces étudiants. Ainsi chaque année ajoute sa nouvelle part d’abandons, de redoublements ou de réorientations.

Pour lutter contre ça, il est nécessaire de faire face à chacunes des difficultés financières que peuvent subir les étudiants en les accompagnant et en les dirigeant vers des solutions adéquates à leurs situations toutes aussi différentes les unes des autres. C’est de là que la création du site est née, ainsi, son principe est de filtrer les différents cas de précarité étudiante afin d’offrir un meilleur confort à chacun et permettre ainsi à l’ensemble des étudiants de trouver un solution à son problème.

Pour utiliser le site, c’est simple, il vous suffit de remplir un formulaire correspondant à la situation de précarité qui vous arrive, qu’elle concerne un problème de transport, de santé, de logement ou d’aide financière, et un algorithme se chargera de vous trouver la solution la plus adéquate. 

Voici les fonctionnalitées pour le défi de la nuit 2019:
 - Une aide à la bourse.
 - Une aide à la santé.
 - Une aide au logement.

 Celles pour le défi de selfie: 
  - Une vidéo qui capture une photo après 3 secondes suivants la pression de bouton.
  - La possibilité de mettre l'interface en plein écran.
  - La possibilité de retourner la caméra.
  - La possibilité de télécharger la photo prise.

Pour le défi de dockerisation, aller sur la branche William-docker en allant sur ce lien: https://gitlab.com/mathis.verleene/polto_gang/tree/William-docker